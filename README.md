Arduino IDE Integration
=======================

README can be found in the docs folder for more details.

Simply put, one would install this in their documents folder, going to Arduino and then going to (or creating) the hardware folder.

After this, you can select the CEENBoT under boards, like you had another Arduino board.

Credit goes to the writer at maniacbug for a re-hacked version of the ATMega324P (same pins as ATMega1284P) to fit my needs. URL:
http://maniacbug.wordpress.com/2011/11/27/arduino-on-atmega1284p-4/

All rights reserved. The hacked core for the ATMega1284 was used as above, under the MIT license. The CEENBoT API for the C programming language is originally licensed under BSD 3 clause (listed at the top of each file.)