/*
	Copyright (c) 2013 ManiacBug / Arduino

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
*/
#ifndef Pins_Arduino_h
#define Pins_Arduino_h

#include <avr/pgmspace.h>

#define NUM_DIGITAL_PINS            32
#define NUM_ANALOG_INPUTS           8
#define analogInputToDigitalPin(p)  ((p < 8) ? (p) + 24 : -1)
#define digitalPinHasPWM(p)         ((p) == 4 || (p) == 5 || (p) == 19 || (p) == 20 || (p) == 21 || (p) == 18)

const static uint8_t SS   = 5;
const static uint8_t MOSI = 6;
const static uint8_t MISO = 7;
const static uint8_t SCK  = 8;

const static uint8_t SDA = 23;
const static uint8_t SCL = 22;
const static uint8_t LED_BUILTIN = 20;

const static uint8_t A0 = 40;
const static uint8_t A1 = 39;
const static uint8_t A2 = 38;
const static uint8_t A3 = 37;
const static uint8_t A4 = 36;
const static uint8_t A5 = 35;
const static uint8_t A6 = 34;
const static uint8_t A7 = 33;

// Only pins available for RECEIVE (TRANSMIT can be on any pin):
// I've deliberately left out pin mapping to the Hardware USARTs
// and also the analog pins: this already makes for 20 RECEIVE pins !

//TODO: UART BELOW double check pins - KD
#define digitalPinToPCICR(p)    (((p) >= 4 && (p) <= 23) ? (&PCICR) : ((uint8_t *)0))
#define digitalPinToPCICRbit(p) ( ((p) == 5 || (p) == 6 || (p) == 9 || (p) == 10) ? 3 : \
                                ( ((p) >= 11 && (p) <= 18) ? 1 : 2 ))
#define digitalPinToPCMSK(p)    ( ((p) == 5 || (p) == 6 || (p) == 9 || (p) == 10) ? (&PCMSK3) : \
                                ( ((p) >= 11 && (p) <= 18) ? (&PCMSK1) : \
                                ( ((p) >= 4 && (p) <= 23) ? (&PCMSK2) : ((uint8_t *)0) )))

#define digitalPinToPCMSKbit(p) ( ((p) == 5 || (p) == 6) ? (p)-1 : \
                                ( ((p) == 9 || (p) == 10) ? (p)-3 : \
                                ( ((p) == 4) ? (p)-2 : \
                                ( ((p) == 7 || (p) == 8) ? (p)-4 : \
                                ( ((p) >= 11 || (p) <= 13) ? (p)-6 : \
                                ( ((p) >= 14 || (p) <= 18) ? (p)-14 : \
                                ( ((p) == 19 || (p) == 20) ? (p)-19 : \
                                ( ((p) >= 21 || (p) <= 23) ? (p)-16 : 0) )))))))

#ifdef ARDUINO_MAIN

// these arrays map port names (e.g. port B) to the
// appropriate addresses for various functions (e.g. reading
// and writing)
const uint16_t PROGMEM port_to_mode_PGM[] = {
        NOT_A_PORT,
        (uint16_t) &DDRA,
        (uint16_t) &DDRB,
        (uint16_t) &DDRC,
        (uint16_t) &DDRD,
};

const uint16_t PROGMEM port_to_output_PGM[] = {
        NOT_A_PORT,
        (uint16_t) &PORTA,
        (uint16_t) &PORTB,
        (uint16_t) &PORTC,
        (uint16_t) &PORTD,
};

const uint16_t PROGMEM port_to_input_PGM[] = {
        NOT_A_PORT,
        (uint16_t) &PINA,
        (uint16_t) &PINB,
        (uint16_t) &PINC,
        (uint16_t) &PIND,
};

const uint8_t PROGMEM digital_pin_to_port_PGM[] = {
        PB,             //1
        PB,             //2
        PB,             //3
        PB,             //4
        PB,             //5
        PB,             //6
        PB,             //7
        PB,				//8
        NOT_A_PIN,
        NOT_A_PIN,
        NOT_A_PIN,
        NOT_A_PIN,
        NOT_A_PIN,
        PD,				//14
        PD,             //15
        PD,             //16
        PD,             //17
        PD,             //18
        PD,             //19
        PD,             //20
        PD,             //21
        PC,             //22
        PC,             //23
        PC,             //24
        PC,             //25
        PC,             //26
        PC,             //27
        PC,				//28
        PC,				//29            
        NOT_A_PIN,
        NOT_A_PIN,
        NOT_A_PIN,
        PA,             //33
        PA,             //34
        PA,             //35
        PA,             //36
        PA,             //37
        PA,             //38
        PA,             //39
        PA, 			//40   
};

const uint8_t PROGMEM digital_pin_to_bit_mask_PGM[] = {
        _BV(0),         // PB0 -  D0 / RX
        _BV(1),         // PB1 -  D1 / TX
        _BV(2),         // PB2 -  D2 / RX2
        _BV(3),         // PB3 -  D3 / TX2
        _BV(4),         // PB4 -  D4
        _BV(5),         // PB5 -  D5 / PWM1
        _BV(6),         // PB6 -  D6 / PWM2
        _BV(7),         // PB7 -  D7
        NOT_A_PIN,
        NOT_A_PIN,
        NOT_A_PIN,
        NOT_A_PIN,
        NOT_A_PIN,
        _BV(0),         // PC4 -  D8
        _BV(1),         // PD6 -  D9 / PWM3
        _BV(2),         // PD7 - D10 / PWM4
        _BV(3),         // PB5 - D11 / MOSI
        _BV(4),         // PB6 - D12 / MISO
        _BV(5),         // PB7 - D13 / SCK / LED_BUILTIN
        _BV(6),         // PB0 - D14
        _BV(7),         // PB1 - D15
        _BV(0),         // PB2 - D16
        _BV(1),         // PB3 - D17 / PWM5
        _BV(2),         // PB4 - D18 / PWM6 / SS
        _BV(3),         // PC0 - D19 / SCL
        _BV(4),         // PC1 - D20 / SDA
        _BV(5),         // PC5 - D21
        _BV(6),         // PC6 - D22
        _BV(7),         // PC7 - D23
        NOT_A_PIN,
        NOT_A_PIN,
        NOT_A_PIN,
        _BV(7),         // PA0 - D24 / A0
        _BV(6),         // PA1 - D25 / A1
        _BV(5),         // PA2 - D26 / A2
        _BV(4),         // PA3 - D27 / A3
        _BV(3),         // PA4 - D28 / A4
        _BV(2),         // PA5 - D29 / A5
        _BV(1),         // PA6 - D30 / A6
        _BV(0),         // PA7 - D31 / A7
};

const uint8_t PROGMEM digital_pin_to_timer_PGM[] = {
        NOT_ON_TIMER,
        NOT_ON_TIMER,
        NOT_ON_TIMER,
        TIMER0A,			//PIN 4        
        TIMER0B,        	//PIN 5
        NOT_ON_TIMER,
        NOT_ON_TIMER,
        NOT_ON_TIMER,
        NOT_ON_TIMER,        
        NOT_ON_TIMER,        
        NOT_ON_TIMER,
        NOT_ON_TIMER,
        NOT_ON_TIMER,
        NOT_ON_TIMER,
        NOT_ON_TIMER,
        NOT_ON_TIMER,
        NOT_ON_TIMER,        
        TIMER1B,        	 //PIN 18
        TIMER1A,			 //PIN 19
        TIMER2B,             //PIN 20, PD6
        TIMER2A,			 //PIN 21
        NOT_ON_TIMER,
        NOT_ON_TIMER,
        NOT_ON_TIMER,
        NOT_ON_TIMER,
        NOT_ON_TIMER,
        NOT_ON_TIMER,
        NOT_ON_TIMER,
        NOT_ON_TIMER,
        NOT_ON_TIMER,
        NOT_ON_TIMER,
        NOT_ON_TIMER,
        NOT_ON_TIMER,
        NOT_ON_TIMER,
        NOT_ON_TIMER,
        NOT_ON_TIMER,
        NOT_ON_TIMER,
        NOT_ON_TIMER,
        NOT_ON_TIMER,
        NOT_ON_TIMER,
};

#endif

#endif