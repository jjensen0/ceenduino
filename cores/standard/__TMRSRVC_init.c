/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
// Auth: Jose Santos
// Desc: Implementation file for '__TMRSRVC_init()' function.

#include "tmrsrvc324v221.h"
#include "__tmrsrvc324v221.h"

void __TMRSRVC_init( void )
{

    // Initialize the timer hardware.  We're assigning timer 0 of the 324 MCU.

    // Set timer0 mode.
    SBV( WGM01, TCCR0A );   // Here WGM01 = 1: count to OCRA (CTC mode).

    // Set clock prescaler for timer 0.
    SBV( CS02,  TCCR0B );   // Here CS02 = 1, CS01 = 0, CS00 = 0: This sets up
                            // the clock prescaler to 256.

    // Set compare-match value for generating interrupts.
    OCR0A = 78;             // At CPU Clock frequency of 20MHz, this will gene-
                            // rate an interrupt every:
                            //
                            //  1 / [ ( 20MHz / 256 ) / 78 ] = 0.9984ms.
                            //
                            //  That's ~1001 interrupts per second.
                            //
                            //  This provides a 'milli-second' relative
                            //  error of approx. 0.16%.

    // That's all.

} // end __TMRSRVC_init()
