/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
// Auth: Jose Santos
// Desc: Implementation file for the 'I2C_SLVE_GetMasterReq()' function.

#include "i2c324v221.h"
#include "__i2c324v221.h"

I2C_STATUS I2C_SLVE_GetMasterReq( void )
{

    I2C_STATUS rval;

    // We only do this if the MASTER is not busy doing something.
    if ( I2C_params.master_active != TRUE )
    {

        // We're only interested in one of the following status codes.
        switch( __I2C_STATUS( TWSR ) )
        {

            // Either the MASTER wants to RECEIVE (ie., Slave-Transmitter)...
            case __I2C_STAT_ST_SLAR_ACK_OK:
            case __I2C_STAT_ST_DATA_ACK_OK:

                rval = I2C_STAT_MASTER_WANTS_TO_RECV;

            break;

            // Or, the MASTER wants to SEND (i.e., Slave-Receiver)...
            case __I2C_STAT_SR_SLAW_ACK_OK:
            case __I2C_STAT_SR_DATA_ACK_OK:

                rval = I2C_STAT_MASTER_WANTS_TO_SEND;

            break;

            // Or, a transaction HAS completed.
            case __I2C_STAT_SR_START_STOP_REQ:
            case __I2C_STAT_ST_DATA_NO_ACK:

                rval = I2C_STAT_MASTER_IS_FINISHED;

            break;

            // Or we don't know... or haven't categorized this condition...
            default:

                rval = I2C_STAT_MASTER_UNKNOWN_REASON;

        } // end switch()

    } // end if()

    else

        rval = I2C_STAT_MASTER_BUSY;

    return rval;

} // end I2C_SLVE_GetMasterReq()
