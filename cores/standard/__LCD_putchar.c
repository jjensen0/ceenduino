/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
// Auth: Jose Santos
// Desc: Implementation file for the [new] '__LCD_putchar()' function.

#include "lcd324v221.h"
#include "__lcd324v221.h"

// ========================== private prototypes ============================ //
static inline BOOL __LCD_process_special_char( char c, BOOL *p_has_changed );

// ============================== functions ================================= //
int __LCD_putchar( char c, FILE *stream )
{

    BOOL not_special = FALSE;   // We start by assuming the character 'c'
                                // is _NOT_ a special character.

    BOOL has_changed = FALSE;   // 'TRUE' if the LCD has changed.

    unsigned char bitmap_data;                                

    // First determine if we're dealing with a 'special character'.  If
    // so process it accordingly.  Also, let us know if the special character
    // results in a change in the contents of the LCD display.
    not_special = __LCD_process_special_char( c, &has_changed );

    // Set the page and column address of the LCD to the current values
    // stored in 'LCD_params'.  These always contain the position to
    // be written _next_.
    LCD_set_PGC_addr( LCD_params.curr_page, LCD_params.curr_col );

    // Finally, if we don't have a special character, then copy the
    // character font's bit pattern from the character look-up-table (LUT)
    // and transfer it to the LCD for display.
    if( not_special )
    {

        // Copy the font bitmap data to the LCD for the particular character.
        unsigned char col = 0;
        for( col = 0; col < LCD_FONTPIXWIDTH; col++ )
        {

            // Get the character bit-pattern from flash-space.
            bitmap_data = pgm_read_byte_near( 
            
                &char_bitmap[ ( ( c - 0x20 ) * LCD_FONTPIXWIDTH ) + col + 3 ] );

            // Write it to the LCD.
            LCD_write_data( bitmap_data );

        } // end for()

        // Write a blank line (separator).
        LCD_write_data( 0x00 );

        // Update the column marker.
        LCD_params.curr_col += ( LCD_FONTPIXWIDTH + 1 );

        // Check if we should 'newline'.
        if ( ( LCD_params.curr_col + LCD_FONTPIXWIDTH ) > LCD_PIX_WIDTH )
        {

            LCD_params.curr_col = 0;
            --LCD_params.curr_page;

        } // end if()

        // Note LCD contents have changed.
        has_changed = TRUE;

    } // end if()

    // Make sure the page marker stays within limits.
    LCD_params.curr_page &= 0x03;

    // Finally, if the user has registered an LCD notify-on-change
    // function, we need to trigger it.
    if( has_changed == TRUE )
    {

        if ( ( LCD_params.lcd_change_notify == TRUE ) &&
             ( LCD_params.p_change_notify_func != NULL ) )

            // Trigger the user's function.
            LCD_params.p_change_notify_func();

    } // end if()

    return 0;

} // end __LCD_putchar()

// ================== internal helper inline functions ====================== //
BOOL __LCD_process_special_char( char c, BOOL *p_has_changed ) 
{

    BOOL not_special = FALSE;

    // Determine if we have a 'special character'.
    switch( c )
    {

        // If 'newline character'.
        case '\n':

            LCD_params.curr_col = 0;
            --LCD_params.curr_page;


        break;

        // If 'carriate return'.
        case '\r':

            // Just move the cursor to the beginning of the current line.
            LCD_params.curr_col = 0;

        break;

        // If 'form feed'.
        case '\f':

            // Just move the cursor down one line.
            --LCD_params.curr_page;

        break;

        // If '\e', we clear the display.
        case '\e':

            LCD_clear();

            // Note LCD contents have changed.
            *p_has_changed = TRUE;

        break;

        // If '\t', we erase until the end of line and also newline.
        //
        // NOTE: Any person that writes C/C++/JAVA programs knows \t stands
        //       for 'TAB' character.  However, for historical reasons, we
        //       choose to retain the '\t' to mean erase till the end of line.
        //
        case '\t':

            for( ; LCD_params.curr_col < LCD_PIX_WIDTH; ++LCD_params.curr_col )

                // Clear with blank data.
                LCD_write_data( 0x00 );


            // Newline.
            LCD_params.curr_col = 0;
            --LCD_params.curr_page;

            // Note LCD contents has changed.
            *p_has_changed = TRUE;

        break;

        // If '\b', we backspace on the current line.
        case '\b':

            if ( LCD_params.curr_col != 0 )

                LCD_params.curr_col -= ( LCD_FONTPIXWIDTH + 1 );

        break;

        // If '\a', we do 'bell' (beep).
        case '\a':

            // Open the SPRK subsystem in 'BEEP'  mode.
            SPKR_open( SPKR_BEEP_MODE );

            // Beep for 500ms w/ 100% note duration.
            SPKR_play_beep( 500, 125, 100 );

            // Close the SPKR beep mode.
            SPKR_close( SPKR_BEEP_MODE );

        break;

        // Otherwise, we have a 'standard character'.
        default:

            not_special = TRUE; // It is 'TRUE' that the character is _NOT_
                                // a special character.

    } // end switch()

    return not_special;

} // end __LCD_process_special_char()
