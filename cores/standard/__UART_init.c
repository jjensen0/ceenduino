/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
// Auth: Jose Santos
// Desc: Implementation file for '__UART_init()' function.

#include "uart324v221.h"
#include "__uart324v221.h"

void __UART_init( UART_ID which )
{

    // Acquire Pin Resources depending on the specified UART device.
    switch( which )
    {

        case UART_UART0:

            // For UART0 we need PD1 (TXD0) and PD0 (RXD0).
            SBD( D, 1, OUTPIN );    // PD1 - TXD0.
            SBD( D, 0, INPIN  );    // PD0 - RXD0.

            // Should set the output to HIGH since this is the
            // 'inactive' state for UART comm.
            SBV( 1, PORTD );

        break;

        case UART_UART1:

            // For UART1 we need PD3 (TXD1) and PD2 (RXD1).
            SBD( D, 3, OUTPIN );    // PD3 - TXD1.
            SBD( D, 2, INPIN  );    // PD2 - RXD1.

            // Should set the output to HIGH since this is the 
            // 'inactive' state for UART comm.
            SBV( 3, PORTD );

        break;

    } // end switch()

    // That's it, gentlemen.
    
} // end __UART_init()
