/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
// Auth: Jose Santos
// Desc: Implementation file which contains the predefined 'SPI_CFGR()' 
//       declarations.  These are custom SPI configuration functions.

#include "spi324v221.h"

// ================ custom SPI configuration routines ======================= //
SPI_CFGR( SPI_default_config )
{

    // Setup the SPCR register (see data-sheet of ATmega324 for details).
    SPCR = ( 1 << SPE ) | ( 1 << MSTR ) | ( 1 << SPR1 ) | ( 1 << SPR0 );

    // Set up clock divider (Fosc/64).
    SPSR = ( 1 << SPI2X );

    // Wait a bit.
    DELAY_us( 2 );

} // end SPI_default_config()
// -------------------------------------------------------------------------- //
SPI_CFGR( SPI_ATtiny0_config )
{

    // Setup the SPCR register (see data-sheet of ATmega324 for details).
    SPCR = ( 1 << SPE ) | ( 1 << MSTR ) | ( 1 << SPR1 ) | ( 1 << SPR0 );

    // Setup up clocking for Fosc/128 divider.
    SPSR = 0;

    // Wait a bit.
    DELAY_us( 100 );
     
} // end SPI_ATtiny0_config() 
// -------------------------------------------------------------------------- //
SPI_CFGR( SPI_PSXC_config )
{

    // Setup the SPCR register (see data-sheet of ATmega324 for details).
    SPCR = ( 1 << SPE  ) | ( 1 << DORD ) | ( 1 << MSTR ) | ( 1 << CPOL ) |
           ( 1 << CPHA ) | ( 1 << SPR1 ) | ( 1 << SPR0 );

    // Setup clock divider (Fosc/128).
    SPSR = 0;

    // Wait a bit.
    DELAY_us( 100 );

} // end SPI_PSXC_confg()
// -------------------------------------------------------------------------- //
SPI_CFGR( SPI_LCD_config )
{

    // Setup the SPCR register (see data-sheet of ATmega324 for details).
    SPCR = ( 1 << SPE ) | ( 1 << MSTR ) | ( 1 << SPR1 ) | ( 1 << SPR0 );

    // Set up clock divider (Fosc/64).
    SPSR = ( 1 << SPI2X );

    // Wait a bit.
    DELAY_us( 2 );

} // end SPI_LCD_config()
// -------------------------------------------------------------------------- //
SPI_CFGR( SPI_FLASH_config )
{

    // TODO: I think we can go way faster... Come back here and try at
    //       full speed possible.

    // Setup the SPCR register ( see data-sheet of ATmega324 for details).
    SPCR =  ( 1 << SPE ) | ( 1 << MSTR ) | ( 1 << SPR1 ) | ( 1 << SPR0 );


    // Set up clock divider (Fosc/64).
    SPSR = ( 1 << SPI2X );

    // Wait a bit.
    DELAY_us( 100 );

} // end SPI_FLASH_config()
