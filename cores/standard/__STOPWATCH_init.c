/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
  //Auth Jose Santos
#include "swatch324v221.h"
#include "__swatch324v221.h"

void __STOPWATCH_init( void )
{

    // We're not using any ports, so we don't need to set up any port bits.
    // However we'll need to set up timer1, which is the 16-bit timer in the
    // ATmega324.

    // Initialize timer 1 hardware:
    TCCR1A = 0x00;          // Reset previous mode.

    TCCR1B = 0x00;          // Reset previous mode.
    SBV( WGM12, TCCR1B );   // Enable 'CTC' mode.
    SBV( CS10, TCCR1B  );   // Set clock-divider to divide by 1.

    // Reset the counter register.
    TCNT1 = 0x0000;

    // Set the compare-match value needed to generate 1us interrupt-rate.
    OCR1AH = 0;
    OCR1AL = 200;   // At 20MHz, this will generate an interrupt every:
                    //
                    // 1 / [ ( 20Mhz / 1 ) / 200 ] = 10us.
    

    // That's it for setup.


} // end __STOPWATCH_open()
