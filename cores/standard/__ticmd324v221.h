/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
// Auth: Jose Santos
// Desc: Header file for the predefined supported command set for the TI
//       calculator functions.  It is kept in a different file to keep it 
//       separated from the work performed by Nick Wertzberger, whom is the
//       author of the TI interface.

// Notes:  The list data format is as follows.  The primary 'intercepting func-
//         tion is always '__TI_Send()'.  This function will receive a list
//         of 'int16_t' elements whose size is determine by 'len'.  The value
//         of the first element (p_list[ 0 ]) represents the 'command' by which
//         we discern which command is to be executed.  This value alone is used
//         to determine which 'sub-function' to invoke.  We pass the same list
//         and length information to the sub-function and let the sub-function
//         take care of the rest.  The sub-function should never read the first
//         element in 'p_list' since at this point it is irrelevant.

#ifndef __TICMD324V221_H__
#define __TICMD324V221_H__

#include "sys324v221.h"
#include "utils324v221.h"

#include "led324v221.h"
#include "step324v221.h"
#include "tmrsrvc324v221.h"
#include "lcd324v221.h"
#include "tiny324v221.h"
#include "spkr324v221.h"
#include "swatch324v221.h"
#include "usonic324v221.h"

// =========================== private defines ============================== //
#define __TI_PIN_INPORT  PINA   /* Port used for TI control/data pins (IN)  */
#define __TI_PIN_OUTPORT PORTA  /* Port used for TI control/data pins (OUT) */
#define __TI_PIN_IN_PORT PINA
#define __TI_PIN_OUT_PORT PORTA

#define __RED_IN_PIN    7   /* Assigned to PA7 */
#define __RED_OUT_PIN   5   /* Assigned to PA5 */

#define __WHITE_IN_PIN  6   /* Assigned to PA6 */
#define __WHITE_OUT_PIN 4   /* Assigned to PA4 */

#define __GET_RED_WIRE() (( GBV( __RED_IN_PIN,   __TI_PIN_INPORT )) ^ ( 1 ))

#define __GET_WHITE_WIRE() (( GBV( __WHITE_IN_PIN, __TI_PIN_INPORT )) ^ ( 1 ))

// Motion commands:
#define __TI_CMD_STOP                   0
#define __TI_CMD_RUN                    1
#define __TI_CMD_STEPWT                 2
#define __TI_CMD_TANKTURNWT             3
#define __TI_CMD_RUNANDBUMP             4
#define __TI_CMD_STEPANDBUMP            5

// Utility commands:
#define __TI_CMD_DELAY_MS           10
#define __TI_CMD_LED                11
#define __TI_CMD_SET_ACCEL          12
#define __TI_CMD_GET_SWIR           13
#define __TI_CMD_SET_SPEED          14
#define __TI_CMD_SET_DIR            15
#define __TI_CMD_SET_STEPS          16
#define __TI_CMD_SET_RUNMODE        17
#define __TI_CMD_SET_RC_SERVO       18
#define __TI_CMD_PLAY_BEEP_PATTERN  19
#define __TI_CMD_PING               20

// Event commands:
#define __TI_CMD_WAIT_ON_BUMP   30
#define __TI_CMD_WAIT_ON_SWITCH 31

// Test commands:
#define __TI_CMD_TEST       2000

#define TI_BUFFER_SIZE      6   // NOTE:  This value MUST match the value for
                                //        'MAX_LIST_SIZE' in '__ti324v221.h'.

// Desc: The following macro is used to return a value that will eventually
//       be retrieved via a 'Get()' call from the calculator.  It is NOT
//       to be confused with TI_return() macro which calls
//       'TI_complete_get_call()'.  Here the purpose is to simply set the
//       parameter in the 'TI_params' structure accordingly and hide this
//       detail for clarity.
#define __TI_return( r )    ( TI_params.get_return_val = ( r ) )

// ============================ private declarations ======================== //
// The following declares the structure for parameters of the TI-subsystem
// module.
typedef struct TI_PARAMS_TYPE {

    volatile BOOL buffer_overrun;   // Data arrived while previous command
                                    // has not yet been dispatched.

    volatile BOOL pending_dispatch; // If 'TRUE', then you cannot overwrite the
                                    // 'data_buffer' because there are commands
                                    // awaiting to be executed.

    volatile BOOL     get_called;       // Indicates the 'get' callback
                                        // has been received, and therefore
                                        // it must be completed via 
                                        // 'TI_Get_return()'.

    volatile int16_t  get_return_val;

    volatile uint8_t  data_len;     // Length of 'valid' elements in the buffer.

    volatile uint16_t data_buffer[ TI_BUFFER_SIZE ]; // The buffer itself.

} TI_PARAMS;



// ============================ private prototypes ========================== //
// Desc: This is the default 'Send' function that gets registered via
//       'TI_open()' if the user passes 'NULL' to the 'send' function pointer
//       parameter.  The purpose of this function is to intercept the command
//       from the calculator and pass it on to the main event loop that takes
//       care of dispatching the function.  This is needed because '__TI_Send()'
//       should not block, and it should return as fast as possible.  So all we
//       can do is read the command and pass it on to the dispatcher.
extern void __TI_Send( int16_t *p_list, uint8_t len );
// -------------------------------------------------------------------------- //
// Desc: This is the default 'Get' function that gets registered via
//       'TI_open()' if the user passes 'NULL' to the 'get' function pointer
//       parameter.
extern void __TI_Get( void );
// -------------------------------------------------------------------------- //
// Desc: This prints a message on the LCD stating that there aren't enough
//       parameters supplied.  That is, the command WAS recognized, but the
//       user failed to send all necessary parameters.
extern void __TI_print_param_error_msg( void );
// -------------------------------------------------------------------------- //
// Desc: Do MCU-specific initialization.
extern void __TI_init( void );
// -------------------------------------------------------------------------- //
// Desc: Do MCU-specific release.
extern void __TI_close( void );
// -------------------------------------------------------------------------- //
// Desc: Enables interrupts on the 'input' pins for incoming TI data.
extern void __TI_port_listen( void );
// -------------------------------------------------------------------------- //
// Desc: Disable interrupts on the 'input' pins for the incoming TI data.
extern void __TI_port_mute( void );

// =========================== TI command set =============================== //
// Desc: This command is invoked automatically for unknown commands.
extern void __TI_CMD_unknown( TI_PARAMS *p_TI_params );
// -------------------------------------------------------------------------- //
// Desc: This command-function will stop motor motion.
//
// Required parameters:
//
//  p_list[ 0 ] = <assigned-command-value> (See '#defines' above).
//  p_list[ 1 ] = <which>,   0 = left, 1 = right, 2 = both.
//  p_list[ 2 ] = <brkmode>, 0 = brakes off, 1 = brakes on.
extern void __TI_CMD_stop( TI_PARAMS *p_TI_params );
// -------------------------------------------------------------------------- //
// Desc: This command-function will issue a motion command in 'free-running'
//       mode.
//
// Required parameters:
//
//  p_list[ 0 ] = <assigned-command-value> (See '#defines' above).
//  p_list[ 1 ] = <which>,  0 = left, 1 = right, 2 = both.
//  p_list[ 2 ] = <dir>,    0 = forward, 1 = reverse.
//  p_list[ 3 ] = <speed>,  Must be between 0 to 400 (in steps/sec).

extern void __TI_CMD_run( TI_PARAMS *p_TI_params );
// -------------------------------------------------------------------------- //
// Desc: This command-function will issue a motion command in 'step-mode'.
//       This function will BLOCK until the motion completes.
//
// Required parameters:
//
//  p_list[ 0 ] = <assigned-command-value> (See '#defines' above).
//  p_list[ 1 ] = <which>, 0 = left, 1 = right, 2 = both.
//  p_list[ 2 ] = <dir>,   0 = forward, 1 = reverse.
//  p_list[ 3 ] = <steps>, Must be between 0 and 65535.
//  p_list[ 4 ] = <speed>, Must be between 0 to 400 (in steps/sec).
//  p_list[ 5 ] = <brkmode>, 0 = brakes OFF on complete, 1 = brakes ON on compl.
//
extern void __TI_CMD_stepwt( TI_PARAMS *p_TI_params );
// -------------------------------------------------------------------------- //
// Desc: This command-function will allow the CEENBoT to turn either left
//       or right a finite number of steps.  The function will BLOCK until
//       the motion completes.
//
// Required parameters:
//
//  p_list[ 0 ] = <assigned-command-value> (See '#defines' above).
//  p_list[ 1 ] = <which_way>, 0 = turn left, 1 = turn right.
//  p_list[ 2 ] = <steps>,   Must be between 0 and 65535.
//  p_list[ 3 ] = <speed>,   Must be between 0 and 400 steps/sec.
//                           NOTE:  The same speed will apply to BOTH steppers.
//  p_list[ 4 ] = <brkmode>, 0 = brakes OFF on complete, 1 = brakes ON on compl.
//
extern void __TI_CMD_tank_turnwt( TI_PARAMS *p_TI_params );
// -------------------------------------------------------------------------- //
// Desc: This command-function will allow the CEENBoT to run in free-running
//       mode until one of its sensors (IR sensors) is 'tripped'.  The 
//       CEENBoT will stop and return WHICH sensor triggered this action after
//       the user issues a Get() after the corresponding Send() that issued
//       *this* command.  This command will BLOCK until a bump action has
//       occurred.
//
// Required parameters:
//
//  p_list[ 0 ] = <assigned-command-value> (See '#defines' above).
//  p_list[ 1 ] = <which>,  0 = left, 1 = right, 2 = both.
//  p_list[ 2 ] = <dir>,    0 = forward, 1 = reverse.
//  p_list[ 3 ] = <speed>,  Must be between 0 to 400 (in steps/sec).
//  p_list[ 4 ] = <timeout>, Must be between 0 and 30 seconds.
//
// Function will return 0 = If NO sensor was tripped (ie., just timed out).
//                      1 = Left sensor was tripped.
//                      2 = Right sensor was tripped.
//                      3 = Both LEFT and RIGHT sensors were tripped.
//
extern void __TI_CMD_run_and_bump( TI_PARAMS *p_TI_params );
// -------------------------------------------------------------------------- //
// Desc: This command-function will allow the CEENBoT to move in step-mode
//       until either one of its sensors (IR sensors) is 'tripped', or until
//       the total number of steps is exhausted.  The CEENBoT will come to 
//       a complete stop when the travel distance (in steps) is exhausted at
//       which point, it will NO longer react to IR triggering.
//
// Required parameters:
//
//  p_list[ 0 ] = <assigned-command-value> (See '#defines' above).
//  p_list[ 1 ] = <which>, 0 = left, 1 = right, 2 = both.
//  p_list[ 2 ] = <dir>,   0 = forward, 1 = reverse.
//  p_list[ 3 ] = <speed>,    Must be between 0 to 400 (in steps/sec).
//  p_list[ 4 ] = <distance>, Must be between 1 and 65535 (in steps).
//
// Function will return 0 = NO sensor was tripped.
//                      1 = Left sensor was tripped.
//                      2 = Right sensor was tripped.
//                      3 = Both LEFT and RIGHT sensor simultaneously tripped.
//
extern void __TI_CMD_step_and_bump( TI_PARAMS *p_TI_params );
// -------------------------------------------------------------------------- //
// Desc: This command will delay the specified number of milliseconds before 
//       anything else happens.
//
// Required parameters:
//
//  p_list[ 0 ] = <assigned-command-value> (See '#defines' above).
//  p_list[ 1 ] = <delay_ms>, Must be between 0 and 32767 milliseconds.
//
extern void __TI_CMD_delay_ms( TI_PARAMS *p_TI_params );
// -------------------------------------------------------------------------- //
// Desc: This command will manipulate the LEDs (ON, OFF, or TOGGLE).
//
// Required parameters:
//
//  p_list[ 0 ] = <assigned-command-value> (See '#defines' above).
//  p_list[ 1 ] = <which>, 0 = Green LED, 1 = Red LED.
//  p_list[ 2 ] = <state>, 0 = LED-OFF, 1 = LED-OFF, 2 = LED-TOGGLE.
//
extern void __TI_CMD_led( TI_PARAMS *p_TI_params );
// -------------------------------------------------------------------------- //
// Desc: This command will set the current acceleration.
//
// Required parameters:
//
//  p_list[ 0 ] = <assigned-command-value> (See '#defines' above).
//  p_list[ 1 ] = <which>, 0 = left, 1 = right, 2 = both.
//  p_list[ 2 ] = <accel>, Must be between 0 and 1000 steps/sec^2.
//
extern void __TI_CMD_set_accel( TI_PARAMS *p_TI_params );
// -------------------------------------------------------------------------- //
// Desc: This command will set the speed of the stepper motors.  The resulting
//       behavior of this command depends on whether the 'BoT is currently set
//       to 'free-running' or 'step mode'.  If in 'free-running' mode, then
//       setting some non-zero speed will make the 'BoT go.  However, if the 
//       'BoT is in 'step mode', then the 'BoT will only go *IF* there are still
//       non-zero steps to cycle through.  The CEENBoT is automatically placed
//       in 'free-running' mode when the command corresponding to the
//       '__TI_CMD_run()' is triggered.  The CEENBoT is automatically placed
//       in 'step-mode' when the command corresponding to the 
//       '__TI_CMD_stepwt()' or '__TI_CMD_turn_tankwt()' are triggered.
//
//       The purpose of this function is to allow the user to 'modulate' the
//       speed of the 'BoT ONCE it is in motion.  This allows 'arc' like
//       turns to be accomplished (instead of the 'tank' turn, which might
//       be found restrictive by some).
//
// Required parameters:
//
//  p_list[ 0 ] = <assigned-command-value> (See '#defines' above).
//  p_list[ 1 ] = <which>, 0 = left, 1 = right, 2 = both.
//  p_list[ 2 ] = <speed_L>, Must be between 0 and ~400 steps/sec.
//  p_list[ 3 ] = <speed_R>, Must be between 0 and ~400 steps/sec.
//
//  Note:  If <which> is equal to ther just LEFT or just RIGHT, just set the
//         parameter of the 'other' wheel to 0 (it is ignored), but you MUST
//         always provide ALL parameters, even if only one wheel is affected
//         by the set speed operation.
extern void __TI_CMD_set_speed( TI_PARAMS *p_TI_params );
// -------------------------------------------------------------------------- //
// Desc: Use this command to set the number of steps when the 'BoT is operating
//       in 'step-mode'.
//
// Required parameters:
//
//  p_list[ 0 ] = <assigned-command-value> (See '#defines' above).
//  p_list[ 1 ] = <which>, 0 = left, 1 = right, 2 = both.
//  p_list[ 2 ] = <steps_L>, Must be between 0 and 65535.
//  p_list[ 3 ] = <steps_R>, Must be between 0 and 65535.
// 
//  Note:  If <which> is equal to ther just LEFT or just RIGHT, just set the
//         parameter of the 'other' wheel to 0 (it is ignored), but you MUST
//         always provide ALL parameters, even if only one wheel is affected
//         by the set steps operation.
extern void __TI_CMD_set_steps( TI_PARAMS *p_TI_params );
// -------------------------------------------------------------------------- //
// Desc: Use this command to set the operating run mode of the 'BoT.  This is
//       only useful when using the 'primitive' motion commands.  The functions
//       '__TI_CMD_stepwt()' and '__TI_CMD_run()' will override it.
//
// TODO: For the time being the run mode will affect BOTH steppers.
//
// Required parameters:
//
//  p_list[ 0 ] = <assigned-command-value>, (See '#defines' above).
//  p_list[ 1 ] = <mode>, 0 = free-running mode, 1 = step mode.
//
extern void __TI_CMD_set_runmode( TI_PARAMS *p_TI_params );
// -------------------------------------------------------------------------- //
// Desc: Use this command to set the position of the a specified RC servo
//       connected to the CEENBoT's RC-servo port.
//
// Required Parameters:
//
//  p_list[ 0 ] = <assigned-command-value>, (See '#defines' above).
//  p_list[ 1 ] = <which_servo>, 0 = RCS0
//                               1 = RCS1
//                               2 = RCS2
//                               3 = RCS3
//                               4 = RCS4
//
//  p_list[ 2 ] = <pos>, Must be a value between 400 and 2100.
//
extern void __TI_CMD_set_RC_servo( TI_PARAMS *p_TI_params );
// -------------------------------------------------------------------------- //
// Desc: Use this command to play a repetitive beep pattern via the CEENBoT's
//       on-board speaker.
//
// Required Parameters:
//
//  p_list[ 0 ] = <assigned-command-value>, (See '#defines' above).
//  p_list[ 1 ] = <beep_freq>, Must be a value between 0 and 500Hz.
//  p_list[ 2 ] = <duration_ms>, Duration time in milli-seconds. (Max is 32767).
//  p_list[ 3 ] = <note_len_ms>, Note duration in milli-seconds. (Max is 32767).
//  p_list[ 4 ] = <repeat>, Number of times to play the pattern (must be a least
//                          1).
//
extern void __TI_CMD_play_beep_pattern( TI_PARAMS *p_TI_params );
// -------------------------------------------------------------------------- //
// Desc: Use this command to emit an 'echo' pulse via a mounted ultra-sonic
//       sensor.  Make sure the 'Parallax' ultrasonic sensor is attached, or
//       otherwise the program _will_ freeze.  You can specify the number of
//       samples to take (and average) before returning the result back to
//       the user once he/she calls 'Get()'.
//
// Required Parameters:
//
//  p_list[ 0 ] = <assigned-command-value>, (See '#defines' above).
//  p_list[ 1 ] = <number-of-samples>
//
// Returns: The user MUST FOLLOW the 'Send()' for this command with a 'Get()'
//          to retrieve the 'distance' in centimeters as perceived by the
//          ultrasonic sensor.  The value is the 'echo distance' in units
//          of 'centimeters' multiplied by 10.  This means, you must divide
//          this value by 10 to get the correct centimeter result.
//      
extern void __TI_CMD_ping( TI_PARAMS *p_TI_params );
// -------------------------------------------------------------------------- //
// Desc: This command will set the direction that each stepper will move
//       when a subsequent 'primitive' motion is being issued -- such as when
//       the the wheels are motioned to move via '__TI_CMD_set_speed()'.
//
// Required parameters:
//
//  p_list[ 0 ] = <assigned-command-value> (See '#defines' above).
//  p_list[ 1 ] = <dir_L>,  0 = Move forward, 1 = Move back. (Left Wheel).
//  p_list[ 2 ] = <dir_R>,  0 = Move forward, 1 = Move back. (Right Wheel).
extern void __TI_CMD_set_dir( TI_PARAMS *p_TI_params );
// -------------------------------------------------------------------------- //
// Desc: This command will allow the user to request the state of the
//       switches and bump sensors.  The user must follow his/her 'Send()'
//       request (on the calculator-side) with a 'Get()' to get the resulting
//       information.
//
// Required parameters:
//
//  p_list[ 0 ] = <assigned-command-value> (See '#defines' above).
//  p_list[ 1 ] = <which>, 0 = Left IR, 1 = Right IR, 
//                         2 = Left or Right IR (whichever is active, if ANY),
//                         3 = S3, 4 = S4, 5 = S5.
//
//                Note: S3, S4, and S5 are the push-button switches on the 
//                      controller board.  Then, for "Left or Right IR", that
//                      means return whether any of these two (or both) are
//                      active.
//
// Returns:  The user MUST FOLLOW the 'Send()' for this command with a 'Get()' 
//           to retrieve the state of the requested sensor or switch.  If the
//           user fails to follow the 'Send()' that initiated the request with
//           a subsequent 'Get()', the return value will be overwritten by
//           any following 'Send()' statements on the calculator program.
//           
//           The values returned will be 0 = inactive, 1 = active (whether 
//           referring to the IR, or any of the Switches).
//                         
extern void __TI_CMD_get_ir_sw( TI_PARAMS *p_TI_params );
// -------------------------------------------------------------------------- //
// Desc: This command allows the user to 'wait' until either the left/right
//       (or either) are triggered.  If no sensor is triggered, the command
//       will wait forever.  There is no timeout period.
//
// Required parameters:
//
//  p_list[ 0 ] = <assigned-command-value> (See '#defines' above).
//  p_list[ 1 ] = <which>, 1 = Left IR ONLY.
//                         2 = Right IR ONLY.
//                         3 = Either IR is triggered.
//
// Returns:  The user MUST FOLLOW the 'Send()' for this command with a 'Get()'
//           to retrieve which sensor was triggered.  In any case 'Get()' will
//           receive one of the following values:
//
//              1 = The LEFT IR was triggered.
//              2 = The RIGHT IR was triggered.
//              3 = BOTH IRs are triggered.
//
extern void __TI_CMD_wait_on_bump( TI_PARAMS *p_TI_params );
// -------------------------------------------------------------------------- //
// Desc: This command is similar to 'wait_on_bump', but instead it waits until
//       a switch is being pressed.  This command will wait forever until
//       a push-button event occurs.
//
// Required Parameters:
//
//  p_list[ 0 ] = <assigned-command-value> (See '#defines' above).
//  p_list[ 1 ] = <which>, 0 = Any switch.
//                         3 = Wait on S3.
//                         4 = Wait on S4.
//                         5 = Wait on S5.
//
// Returns:  The user MUST FOLLOW the 'Send()' with a corresponding 'Get() to
//           retreive which switch was triggered.  In any case, 'Get()' will
//           receive one of the following values:
//
//              3 = S3 was triggered.
//              4 = S4 was triggered.
//              5 = S5 was triggered.
//
extern void __TI_CMD_wait_on_switch( TI_PARAMS *p_TI_params );
// -------------------------------------------------------------------------- //
// Desc: This command can be used to 'test' the communication between the TI
//       calculator and the CEENBoT.
//
// Required parameters:
//
//  p_list[ 0 ] = <assigned-command-value> (See '#defines' above).
//  p_list[ 1 ] = arbitrary value 1.
//  p_list[ 2 ] = arbitrary value 2.
//  p_list[ 3 ] = arbitrary value 3.
//  p_list[ 4 ] = arbitrary value 4.
//  p_list[ 5 ] = arbitrary value 5.
extern void __TI_CMD_test( TI_PARAMS *p_TI_params );

// ======================== external parameters ============================= //
extern TI_PARAMS TI_params;
extern TIMEROBJ  bump_check_timer;
extern TIMEROBJ  timeout_timer;

#endif /* __TICMD324V221_H__ */
