/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
// Auth: Jose Santos
// Desc: Implementation file for 'SPI_open()' function.

#include "spi324v221.h"
#include "__spi324v221.h"

SUBSYS_OPENSTAT SPI_open( void )
{

    SUBSYS_OPENSTAT rval;

    // Set default result values (assume no errors).
    rval.subsys = SUBSYS_SPI;
    rval.state  = SUBSYS_OPEN;

    // Initialize MCU-specific hardware.
    __SPI_init();

    // Assign custom configuration routines.
    SPI_set_config_func( SPI_ADDR_LCD,     SPI_LCD_config );
    SPI_set_config_func( SPI_ADDR_PSXC,    SPI_PSXC_config );
    SPI_set_config_func( SPI_ADDR_ATTINY0, SPI_ATtiny0_config );
    SPI_set_config_func( SPI_ADDR_SPIFLASH, SPI_FLASH_config );

    // Assign a default configuration to the remaining ones.
    SPI_set_config_func( SPI_ADDR_SMARTDEV0, SPI_default_config );
    SPI_set_config_func( SPI_ADDR_SMARTDEV1, SPI_default_config );
    SPI_set_config_func( SPI_ADDR_SMARTDEV2, SPI_default_config );
    SPI_set_config_func( SPI_ADDR_NA,        SPI_default_config );

    // Update the system variable to reflect the SPI is ready.  We need to do
    // this first, or otherwise, 'SPI_set_slave_addr()' won't work here.
    SYS_set_state( SUBSYS_SPI, SUBSYS_OPEN );

    // Set current SPI address to some non-existent address.
    curr_spi_addr = 0xFF;

    // Default to slave device 7.
    SPI_set_slave_addr( SPI_ADDR_NA );

    return rval;

} // end SPI_init()
