/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
// Auth: Jose Santos
// Desc: Implementation file for the 'I2C_SLVE_get()' function.

#include "i2c324v221.h"
#include "__i2c324v221.h"

I2C_STATUS I2C_SLVE_get( unsigned char *pData, BOOL acknowledge )
{

    // Assume everything will go ok.
    I2C_STATUS rval = I2C_STAT_OK;

    // We ONLY continue if the 'MASTER' is not busy doing something (i.e.,
    // the MASTER is not in the middle of an 'active' transaction).
    if ( I2C_params.master_active != TRUE )
    {

        // ** Note that the SLAVE is BUSY, so no MASTER operations should
        //    be allowed to take place.
        I2C_params.slave_active = TRUE;

        // Wait for TWINT to be set (NOTE: it SHOULD be at this point).
        while( ! ( GBV( TWINT, TWCR ) ) );

        // Check if the device has been initially addressed.
        if ( ( __I2C_STATUS( TWSR ) == __I2C_STAT_SR_SLAW_ACK_OK ) ||
             ( __I2C_STATUS( TWSR ) == __I2C_STAT_SR_DATA_ACK_OK ) )
        {

            // What we do next depends on whether we acknowledge or not...
            if ( acknowledge == TRUE )
            {

                // Data WILL be received and acknowledged.
                TWCR = ( 1 << TWINT ) | ( 1 << TWEA ) | ( 1 << TWIE ) |
                       ( 1 << TWEN );

            } // end if()                       

            else

                // Data WILL be received and NOT acknowledged.
                TWCR = ( 1 << TWINT ) | ( 1 << TWIE ) | ( 1 << TWEN );


            // Wait for the data to arrive.
            while( ! ( GBV( TWINT, TWCR ) ) );
            
            // Check what the status code is now ..
            // First check if the data was received and acknowledged.
            if ( __I2C_STATUS( TWSR ) == __I2C_STAT_SR_DATA_ACK_OK )

                // ... just read it.
                *pData = TWDR;

            // Otherwise, check if the data was received and NOT acknowledged.
            else if ( __I2C_STATUS( TWSR ) == __I2C_STAT_SR_DATA_NO_ACK )
            {

                // ... just read it.
                *pData = TWDR;

                // And note that a NOT ACK has been sent.
                rval = I2C_STAT_NO_ACK;

            } // end else-if()

            // Check here if we have a STOP/RESTART condition...
            //
            // FIXME: Not quite convinced about having two 'else-if()' blocks
            //        that test for the same status.  This will have to be
            //        're-conceived' in a different manner.  It'll do for
            //        the time being, however.
            //
            else if ( __I2C_STATUS( TWSR ) == __I2C_STAT_SR_START_STOP_REQ )

                // Return this code so that the user can take appropriate
                // action.
                rval = I2C_STAT_START_STOP_REQ;

            else

                rval = I2C_STAT_UNKNOWN_ERROR;

        } // end if()

        // Check if a STOP/RE-START condition is being issued.
        else if ( __I2C_STATUS( TWSR ) == __I2C_STAT_SR_START_STOP_REQ )

            // Do nothing... return this code so the user can take appropriate
            // action.
            rval = I2C_STAT_START_STOP_REQ;

        // Otherwise we have a problem.
        else

            rval = I2C_STAT_UNKNOWN_ERROR;


    } // end if()

    // Otherwise, note that we can't do anything because the MASTER
    // is active.
    else

        rval = I2C_STAT_MASTER_BUSY;

    return rval;        

} // end I2C_SLVE_get()
