/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
// Auth: Jose Santos
// Desc: Implementation file for 'TMRSRVC_new()' function.

#include "tmrsrvc324v221.h"
#include "__tmrsrvc324v221.h"

TMRNEW_RESULT TMRSRVC_new( TIMEROBJ *pTimerObject, TMR_FLGS validNotifyFlags,
                           TMR_TCMODE tcMode,      TIMER16 nTicks )
{

    TMRNODE *pNode = 0;

    // Set up the default return value.
    TMRNEW_RESULT rval = TMRNEW_ERROR;

    // Only proceed if the timer is OPEN.

    if ( SYS_get_state( SUBSYS_TMRSRVC ) == SUBSYS_OPEN )
    {

        // Start out by creating a 'node' to place the timer object in.
        pNode = ( TMRNODE * ) malloc( sizeof ( TMRNODE ) );

        // Check that the allocation was successful -- if so, proceed.
        if ( pNode != NULL )
        {

            // Set the valid notify bits.  This also resets the flags state
            // into a 'safe state'.
            pTimerObject->flags = validNotifyFlags;

            // If the user-supplied flag is valid, clear it.
            if ( validNotifyFlags & TMRFLG_FLAGNOTIFY )
            {

                // *( pTimerObject->pNotifyFlag ) = 0;

                pTimerObject->tc = 0;

            } // end if()

            // Set up the TCMODE (Terminal Count Mode) in the timer object 
            // without disturbing the current 'valid' bits (bits 1 and 2 in 
            // the 'flags' field of the timer object.
            if ( tcMode == TMR_TCM_RESTART )

                pTimerObject->flags |= 0x04;

            // Set the 'ENABLED' bit (bit 3) by default.
            pTimerObject->flags |= 0x08;

            // Set time request.
            pTimerObject->timeReq = nTicks;

            // Set the [preliminary] number of ticks ( this number may be even-
            // tually adjusted to 'delta ticks'.
            pTimerObject->ticks = nTicks;

            // Fill up the 'node' with data.
            pNode->pTimerObj = pTimerObject;    // Attach the timer object.
            pNode->pNextNode = NULL;            // Mark end-of-list.

            // Set interal status to BUSY.  We don't want the timer subsystem
            // to perform timer updates while we're messing with the list.
            timer_busy_status = TMR_BSY;

            // Go ahead and insert the 'loaded' node with the timer object
            // in the linked list.
            TMRSRVC_insert( pNode );

            // Set internal status to no longer busy.
            timer_busy_status = TMR_NOT_BSY;

            // Indicate we're good.
            rval = TMRNEW_OK;

        } // end if()

        // Otherwise, there was an allocation error.  Indicate this error.
        else
        {

            rval = TMRNEW_OUTOFMEMORY;

        } // end else.

    } // end if( OPEN )

    return rval;

} // end TMRSRVC_new()
