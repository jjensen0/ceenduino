/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
// Auth: Jose Santos
// Desc: Implementation file for the 'I2C_MSTR_get()' function.

#include "i2c324v221.h"
#include "__i2c324v221.h"

I2C_STATUS I2C_MSTR_get( unsigned char *pData, BOOL acknowledge )
{

    // Assume everything is OK.
    I2C_STATUS rval = I2C_STAT_OK;

    // Only do anything if the device is configured.
    if ( I2C_params.configured == TRUE )
    {

        // Also, make sure the slave device is not busy.
        if ( I2C_params.master_active == TRUE )
        {

        
            // **Next initiate the command to get a 'byte'.  How we do 
            //   this depends on whether we need to 'acknowledge' or not.
            if ( acknowledge == TRUE )

                TWCR = ( 1 << TWINT ) | ( 1 << TWEA ) | ( 1 << TWEN );

            // Otherwise do not acknowedge... 
            // (this is sort of a "last byte 'acknowledge'").
            else

                TWCR = ( 1 << TWINT ) | ( 1 << TWEN );

            // Wait for the TWINT to be set.
            while( ! ( GBV( TWINT, TWCR ) ) );

            // **Verify that the data was recieved.

            // If acknowledge was requested...
            if ( acknowledge == TRUE )
            {

                // If data was NOT acknowedged, when it should have been...
                if ( __I2C_STATUS( TWSR ) != __I2C_STAT_MR_DATA_ACK_OK )

                    rval = I2C_STAT_ACK_ERROR;

                // Otherwise...                
                else

                    // Read the data.
                    *pData = TWDR;

            } // end if()

            else
            {

                // If data was ACKNOWLEDGED, when it SHOULDN'T HAVE...
                if ( __I2C_STATUS( TWSR ) != __I2C_STAT_MR_DATA_NO_ACK )

                    rval = I2C_STAT_ACK_ERROR;

                // Otherwise...
                else

                    // Read the data.
                    *pData = TWDR;

            } // end else

        } // end if()

        // Otherwise, note that we're not in MASTER mode as we should be.
        else

            rval = I2C_STAT_NOT_MASTER;

    } // end if()

    else

        rval = I2C_STAT_CONFIG_ERROR;

    return rval;

} // end I2C_MSTR_get()
