/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
// Auth: Jose Santos
// Desc: Implementation file for 'SPIFLASH_open()' function.

#include "spiflash324v221.h"
#include "__spiflash324v221.h"

SUBSYS_OPENSTAT SPIFLASH_open( void )
{

    SUBSYS_OPENSTAT rval;

    // Set the default result values ( assume no errors ).
    rval.subsys = SUBSYS_SPIFLASH;
    rval.state  = SUBSYS_OPEN;

    // Check if the SPI services are available -- if NOT, we can't move on...
    if ( SYS_get_state( SUBSYS_SPI ) == SUBSYS_CLOSED )
    {

        // Mark the error...
        rval.subsys = SUBSYS_SPI;
        rval.state  = SUBSYS_CLOSED;

    } // end if()

    // Otherwise, check if the SPIFLASH is NOT already OPEN...
    else if ( SYS_get_state( SUBSYS_SPIFLASH ) == SUBSYS_CLOSED )
    {

        // Do any MCU-specific initialization.
        __SPIFLASH_init();

        // NOte the system variable that SPIFLASH servies are now
        // ready for use.
        SYS_set_state( SUBSYS_SPIFLASH, SUBSYS_OPEN );

    } // end else-if()

    return rval;

} // end SPIFLASH_open()
