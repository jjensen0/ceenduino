/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
// Auth: Jose Santos
// Desc: Header file for the ATmega324 module (otherwise known also as 'CPU0').

#ifndef __MEGA324V221_H__
#define __MEGA324V221_H__

// ============================= includes =================================== //
#include<avr/io.h>
#include<avr/interrupt.h>

#include "utils324v221.h"
#include "sys324v221.h"

// =========================== defines ====================================== //
#define ATMEGA_NORMAL_STARTUP_WAIT  40
#define ATMEGA_SLEEP_STARTUP_WAIT   28000

// ============================ globals ===================================== //

// Custom enumeration type declaration for specifying the operating speed of
// the primary MCU.
typedef enum CLKMODE_TYPE {

    CLKMODE_SLEEP = 0,
    CLKMODE_20MHZ,
    CLKMODE_2MHZ,
    CLKMODE_1MHZ

} CLKMODE;

// ============================ prototypes ================================== //
// Desc: Function allocates and initializes resources for the primary CPU/MCU.
//       This function will clear the global interrupt flag to inhibit all 
//       interrupts during initialization.  It is up to the user to manually
//       call 'sei()' at the apporiate time to re-enable the interrupts.
extern SUBSYS_OPENSTAT ATMEGA_open( void );
// -------------------------------------------------------------------------- //
// Desc: Function deallocates and releases resources used by the primary 
//       CPU/MCU.
extern void ATMEGA_close( void );
// -------------------------------------------------------------------------- //
extern void ATMEGA_set_clk_mode( CLKMODE clk_mode );
// -------------------------------------------------------------------------- //

#endif /* __MEGA324V221_H__ */
