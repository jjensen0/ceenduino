
/* Programmer: Nicholas Wertzberger
 *      Email: wertnick@gmail.com
 *
 * All work Copyright 2010 Nicholas Wertzberger.
 *
 * This is the TI_send_variable routine for the ti transport layer.
 */

#include <inttypes.h>
#include <stdio.h>
#include <string.h>

#include "ti324v221.h"
#include "__ti324v221.h"

/* input: the name of the var, its length, and the variable to send
 * output: none
 * notes:  89-specific routine for sending data.
 */
void ti_mkvar(packet_t *pk, uint8_t *name, uint8_t len, int16_t var)
{
    uint8_t i=0;

    sprintf((char *)pk->data,"%d", var);    /* so we can calcualte the strlen :\ */

    pk->data[0] = strlen((char *)pk->data)+1;
    pk->data[1] = 0;
    pk->data[2] = 0;
    pk->data[3] = 0;
    pk->data[4] = 0;
    pk->data[5] = len;
    for (i = 0; i < len; i++)
    {
        pk->data[i+6] = name[i];
    }
    pk->len = len + 6;
}

void ti_mkdata(packet_t *pk, int16_t var)
{
    sprintf((char *)pk->data,"%d", var);
    pk->len = strlen((char *)pk->data) + 1;
}

uint8_t ti_getlist(packet_t *pk, int16_t *list, uint8_t len)
{
    uint8_t idx = 0;
    uint8_t i;

    for (i = 0; i < pk->len; i++)
    {
        BOOL negative = FALSE;
        BOOL lt_zero = FALSE;

        /* Look for the first 0x20 */ 
        if (pk->data[i] == 0x20 && idx < len)
        {
            list[idx] = 0;

            /* I was told this is an acceptable C construct for the case
             * where you want to do something every time before validating.
             */
            while (1)
            {
                i++;
                if (pk->data[i] == 0x20 || pk->data[i] == 0x00) {
                    break;
                }

                if (pk->data[i] == '-')
                {
                    negative = TRUE;
                }
                else if (pk->data[i] == '.')
                {
                    lt_zero = TRUE;

                }
                else if (!lt_zero)
                {
                    /* Parse the ascii into an integer (base 10) */
                    /* Step 1: multiply by 10 */
                    list[idx] *= 10;

                    /* step 2: convert ascii to an integer, add. */
                    list[idx] += pk->data[i] & 0x0f;
                }
            }
            if (negative) 
            {
                list[idx] = -list[idx];
            }

            idx++;
            i--; /* we have to go back one byte for the detector to work. */
        }
    }
    return idx;
}
