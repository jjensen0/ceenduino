/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
// Auth: Jose Ssantos
// Desc: Implementation file for 'SPKR_play_song()' function.

#include "spkr324v221.h"
#include "__spkr324v221.h"

// ========================= private prototypes ============================= //
static inline void __SPKR_process_measure( SPKR_MEASURE *pMeasure );
static inline void __SPKR_process_notes( SPKR_PLAYNOTE *pNote );

// ============================ functions =================================== //
void SPKR_play_song( SPKR_SONG *pSong )
{

    unsigned short int i, j;
    unsigned short int num_measures;
    unsigned short int times;


    if ( pSong != NULL )
    {

        // Get the measures.
        SPKR_MEASURE *pMeasures = pSong->pMeasures;

        // Get the number of measures.
        num_measures = pSong->num_measures;

        // Get number of times to play all measures.
        times = pSong->times;

        // Play all measures the specified number of repeats.
        for( i = 0; i < times; ++i )
        {

            // Process each measure.
            for( j = 0; j < num_measures; ++j )

                __SPKR_process_measure( &pMeasures[ j ] );

        } // end for()            

    } // end if()

} // end SPKR_play_song()
// ============================== private functions ========================= //
void __SPKR_process_measure( SPKR_MEASURE *pMeasure )
{

    unsigned short int num_notes;
    unsigned short int times;
    unsigned short int i, j;


    if ( pMeasure != NULL )
    {

        // Get the notes.
        SPKR_PLAYNOTE *pNotes = pMeasure->pNotes;

        // Get the number of notes.
        num_notes = pMeasure->num_notes;

        // Get the number of times to repeat the notes in the measure.
        times   = pMeasure->times;

        for( i = 0; i < times; ++i )
        {

            // Process each set of notes.
            for( j = 0; j < num_notes; ++j )

                __SPKR_process_notes( &pNotes[ j ] );

        } // end for()
    } // end if()

} // end __SPKR_process_measure()
// -------------------------------------------------------------------------- //
void __SPKR_process_notes( SPKR_PLAYNOTE *pNote )
{

    // Just play the note.
    SPKR_play_note( ( SPKR_NOTE ) pNote->note,
                    ( SPKR_OCTV ) pNote->octave,
                    ( signed short int ) pNote->transp,
                    pNote->duration_ms,
                    ( unsigned short int ) pNote->len );
    
} // end __SPKR_process_notes()
