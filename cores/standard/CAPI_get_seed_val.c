/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
// Auth: Jose Santos
// Desc: Impelementation file for the 'CAPI_get_seed_val()' function.

#include "capi324v221.h"

unsigned short int CAPI_get_seed_val( void )
{

    unsigned short int rval = 1;

    ADC_SAMPLE sample;              // NOTE: 'ADC_SAMPLE' is essentially of
                                    //       unsigned short int.

    // If the seed has not yet been generated.
    if ( seed_generated == FALSE )
    {

        // Open the ADC subsystem.
        ADC_open();

        // Use the 5V reference.
        ADC_set_VREF( ADC_VREF_AVCC );

        // Set the channel.
        ADC_set_channel( ADC_CHAN1 );

        // Get a few sample out of th way.
        ADC_sample();
        ADC_sample();

        // Wait a bit.
        DELAY_ms( 10 );

        // Get the _real_ sample:
        sample = ADC_sample();

        ADC_set_channel( ADC_CHAN0 );

        sample += ADC_sample();
        sample += ADC_sample();

        ADC_set_channel( ADC_CHAN7 );

        sample *= ADC_sample();
        sample *= ADC_sample();

        // Multiply this by some random 'odd' value to 
        // minimize the presence of zeros.
        sample *= 57319;

        // Close the ADC subsystem module.
        ADC_close();

        // Save the new seed.
        current_seed_val = sample;

        // Note that the seed has been generated.
        seed_generated = TRUE;

        rval = current_seed_val;

    } // end if()        

    // Otherwise... the seed has already been generated.  Return this value.
    else

        rval = current_seed_val;

    // Return the seed. 
    return rval;
    
} // end CAPI_get_seed_val()
