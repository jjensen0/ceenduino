/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
// Auth: Jose Santos
// Desc: Implementation file for the '__TI_CMD_wait_on_bump()' function.

#include "ti324v221.h"
#include "__ti324v221.h"
#include "__ticmd324v221.h"

void __TI_CMD_wait_on_bump( TI_PARAMS *p_TI_params )
{

    unsigned char which_IR = 0;

    unsigned char sensors = 0;

    LCD_clear();

    // Proceed if we have the minimum number of required parameters.
    if( p_TI_params->data_len > 1 )
    {

        LCD_printf_PGM( PSTR( "Waiting for bump...\n" ) );

        // Enter the infinite loop.
        while( 1 )
        {

            // Read the state of the bump sensors.
            sensors = ATTINY_get_sensors();

            // Determine which sensor(s) to check on, based on
            // what the user wishes to be notified on...
            switch( p_TI_params->data_buffer[ 1 ] )
            {

                // Monitor LEFT IR.
                case 1:

                    // If the LEFT is active...
                    if ( sensors & SNSR_IR_LEFT )

                        // Make a note of it.
                        which_IR = 1;

                break;

                // Monitor RIGHT IR.
                case 2:

                    // If the RIGHT is active...
                    if ( sensors & SNSR_IR_RIGHT )

                        // Make a note of it.
                        which_IR = 2;

                break;

                // Monitor EITHER IR.
                case 3:

                    // Check if both are active.
                    if ( ( sensors & SNSR_IR_LEFT ) &&
                         ( sensors & SNSR_IR_RIGHT ) )

                         which_IR = 3;

                    // Otherwise, check if just the LEFT IR is active...
                    else if ( sensors & SNSR_IR_LEFT )

                        which_IR = 1;

                    // Otherwise, check if just the RIGHT IR is active...
                    else if ( sensors & SNSR_IR_RIGHT )

                        which_IR = 2;

                break;

                default: /* Nothing to do. */ ;

            } // end switch()

            // Check if we need to get out of the infinite loop and 
            // display a message while we're at it.
            if ( which_IR != 0 )
            {

                switch( which_IR )
                {

                    case 1:

                        LCD_printf_PGM( PSTR( "LEFT bump hit!\n" ) );

                    break;

                    case 2:

                        LCD_printf_PGM( PSTR( "RIGHT bump hit!\n" ) );

                    break;

                    case 3:

                        LCD_printf_PGM( PSTR( "DUAL bump hit!\n" ) );

                    break;

                    default: /* Nothing to do. */ ;

                } // end switch()

                break;

            } // end if()                

            // If we haven't exited the loop, then wait ~63ms to 
            // give us an IR status check rate of 16 times/sec.
            TMRSRVC_delay( 63 );

        } // end while()

    } // end if()

    else
    {

        __TI_print_param_error_msg();

    } // end else.

    __TI_return( which_IR );

} // end __TI_CMD_wait_on_bump()
