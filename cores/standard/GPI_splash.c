/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
// Auth: Jose Santos
// Desc: Implementation file for 'GPI_splash()' function.

#include "gpi324v221.h"

void GPI_splash( unsigned short int gpi_rev_major,
                 unsigned short int gpi_rev_minor,
                 unsigned short int gpi_rev_build )
{

    // Only do this if the LCD subsystem has been opened.
    if ( SYS_get_state( SUBSYS_LCD ) == SUBSYS_OPEN )
    {

        LCD_clear();
        LCD_printf_RC( 3, 0, "(c)2011-CEENBoT, Inc." );
        LCD_printf_RC( 2, 0, "  Univ. of Nebraska  " );
        LCD_printf_RC( 1, 0, "API-Rev: v%d.%02d.%03d%c",
                                        CAPI_REV_MAJOR,
                                        CAPI_REV_MINOR,
                                        CAPI_REV_BUILD,
                                        CAPI_REV_STAT );


        if ( ( gpi_rev_major == 0 ) &&
             ( gpi_rev_minor == 0 ) && 
             ( gpi_rev_build == 0 ) )
        {

            LCD_printf_RC( 0, 0, "GPI-Rev: Unknown" );

        } // end if()

        // If we have a non-zero GPI revision, then display it.
        else
        {

            LCD_printf_RC( 0, 0, "GPI-Rev: v%d.%02d.%03d", 
                                        gpi_rev_major,
                                        gpi_rev_minor,
                                        gpi_rev_build );


        } // end else.

        // Display this for a couple of seconds.
        TMRSRVC_delay( TMR_SECS( 3 ) );

        // ...and clear the display afterwards.
        LCD_clear();

    } // end if()

} // end GPI_splash()
