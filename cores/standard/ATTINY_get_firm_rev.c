/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
// Auth: Jose Santos
// Desc: Implementation file for 'ATTINY_get_firm_rev()' function.

#include "tiny324v221.h"

void ATTINY_get_firm_rev( ATTINY_FIRMREV *pDest )
{

    // Firt of all check if the value is not NULL.
    if ( pDest != ( ( void * ) 0 ) )
    {

        // Only do this if the ATtiny is open for business.
        if ( SYS_get_state( SUBSYS_CPU1 ) == SUBSYS_OPEN )
        {

            // Mark message separator (start message sequence).
            SPI_set_slave_addr( SPI_ADDR_NA );

            DELAY_us( 30 );

            // Get the ATTiny's attention.
            SPI_transmit( SPI_ADDR_ATTINY0, ATTMSG_ATTN );

            // Tell it we want Firmware revision info.
            // NOTE: The 'ATTMSG_LONG_FIRM_REV' request the full revision
            //       number (which includes the revision 'state' letter).
            SPI_transmit( SPI_ADDR_ATTINY0, ATTMSG_LONG_FIRM_REV );

            // Get the data.
            DELAY_us( 30 );

            // Get the 'major' revision number.
            pDest->major = SPI_receive( SPI_ADDR_ATTINY0, SPI_NULL_DATA );

            DELAY_us( 30 );

            // Get the 'minor' revision number.
            pDest->minor = SPI_receive( SPI_ADDR_ATTINY0, SPI_NULL_DATA );

            DELAY_us( 30 );

            // Get the 'revision status' ID.
            pDest->status = SPI_receive( SPI_ADDR_ATTINY0, SPI_NULL_DATA );
            
            // DELAY_us( 30 );

            // Mark message separator (end message sequence).
            SPI_set_slave_addr( SPI_ADDR_NA );

        } // end if()

        else
        {

            // Return nothing.
            pDest->major = 0;
            pDest->minor = 0;
            pDest->status = '-';

        } // end else.

    } // end if()

    // That's it.

} // end ATTINY_get_firm_rev()
