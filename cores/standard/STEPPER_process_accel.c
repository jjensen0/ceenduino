/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
// Auth: Jose Santos
// Desc: Implementation file for 'STEPPER_process_accel()' function.

#include "step324v221.h"
#include "__step324v221.h"

void STEPPER_process_accel( unsigned char process_left,
                            unsigned char process_right )
{

    STEPPER_SPEED temp_speed;

    // Process acceleration for left motor.
    if ( process_left )
    {

        // Determine if we're accelerating or decelerating...
        temp_speed.left = STEPPER_params.step_speed.left - 
                                            STEPPER_params.curr_speed.left;

        // If accelerating...
        if ( temp_speed.left > 0 )
        {

            // Increment speed by one.
            STEPPER_params.curr_speed.left++;

        } // end if()

        // Otherwise, if decelerating...
        else if ( temp_speed.left < 0 )
        {

            // Decrement speed by one.
            STEPPER_params.curr_speed.left--;

        } // end else.
        
    } // end if()

    // Process acceleration for right motor.
    if ( process_right )
    {

        // Determine if we're accelerating or decelerating...
        temp_speed.right = STEPPER_params.step_speed.right - 
                                            STEPPER_params.curr_speed.right;

        // If accelerating...
        if ( temp_speed.right > 0 )
        {

            // Increment speed by one.
            STEPPER_params.curr_speed.right++;

        } // end if()

        // Otherwise, if decelerating...
        else if ( temp_speed.right < 0 )
        {

            // Decrement speed by one.
            STEPPER_params.curr_speed.right--;

        } // end else.

    } // end if()

} // end STEPPER_process_accel()
