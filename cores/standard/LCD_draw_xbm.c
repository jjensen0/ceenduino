/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
// Auth: Jose Santos
// Desc: Implementation file for 'LCD_draw_xbm()' function.

#include "lcd324v221.h"
#include "__lcd324v221.h"

void LCD_draw_xbm( const char *p_xbm_data )
{

    unsigned char bitmap_data;
    unsigned int i; // Current page (row).
    unsigned int j; // Current column.
    unsigned int k; // 8-Pixel entry in the array.

    // Set the display RAM to start from the beginning.
    LCD_write_cmd( LCDCMD_DRAM_START_ADDR );

    for( i = 0; i < ( LCD_PIX_HEIGHT / LCD_PAGE_PIXHEIGHT ) ; ++i )
    {

        // Set the page and column address.
        LCD_set_PGC_addr( i, 0 );

        k = i;

        for( j = 0; j < LCD_PIX_WIDTH; ++j, k += 4  )
        {

            // Get the pixel bit-pattern from Program Memory.
            bitmap_data = pgm_read_byte_near( &p_xbm_data[ k ] );

            // Write it to the LCD.
            LCD_write_data( bitmap_data );

        } // end for()

        
    } // end for()

    // Make sure the cursor for any consecutive writes begin at the
    // LCD's origin (top left-hand-corner).
    LCD_set_next_PGC( 3, 0 );
    
    // That's it.

} // end LCD_draw_xbm()
