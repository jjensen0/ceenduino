/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
// Auth: Jose Santos
// Desc: Implementation file for 'TMRSRVC_open()' function.

#include "tmrsrvc324v221.h"
#include "__tmrsrvc324v221.h"

// NOTE: EXCEPTION -- this is normally not something I like to do.
#include "__isr324v221.h"

SUBSYS_OPENSTAT TMRSRVC_open( void )
{

    SUBSYS_OPENSTAT rval;

    // Set up default values to assume there's no error.
    rval.subsys = SUBSYS_TMRSRVC;
    rval.state  = SUBSYS_OPEN;

    // Only do this if the timing service subsystem is not already OPEN.
    if ( SYS_get_state( SUBSYS_TMRSRVC ) == SUBSYS_CLOSED )
    {

        // Because this is a linked list, all we have to do is initialize
        // the timer list variables to 'safe values'.

        tmrNodes.nNodes = 0;      // No nodes currently in the list.
        tmrNodes.pNodeList = 0;   // NULL pointer -- SAFETY FIRST.

        // Set system to NOT busy.
        timer_busy_status = TMR_NOT_BSY;

        // That's it -- we're ready.
        // timer_ready_status = TMR_RDY;

        // Keep the timer stopped by default.
        timer_run_status = TMR_STOPPED;

        // Register the CBOT-ISR to use with this timer service using
        // the 'unrestricted' version of the 'attach' function.
        __ISR_attach( ISR_TIMER0_COMPA_VECT, __TIMER0_COMPA_vect );

        // Initialize the MCU-specific hardware.
        __TMRSRVC_init();

        // Start the MCU-specific timer hardware.
        __TMRSRVC_start();

        // Indicate the timer service subsystem is open for business.
        SYS_set_state( SUBSYS_TMRSRVC, SUBSYS_OPEN );
        
    } // end if()

    return rval;

} // end TMRSRVC_open()
