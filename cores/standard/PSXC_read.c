/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
// Auth: Jose Santos
// Desc: Implementation file for 'PSXC_read()' function.

#include "psxc324v221.h"
#include "__psxc324v221.h"

BOOL PSXC_read( PSXC_STDATA *pStatus_data )
{

    unsigned int i;

    // Assume the data obtained from the controller is 'good'.
    BOOL rval = TRUE;

    // If the PSXC subsystem is not open then we can't continue.
    if ( ( SYS_get_state( SUBSYS_PSXC ) == SUBSYS_OPEN ) && 
          ( pStatus_data != NULL ) )
    {


        // Prepare to send and request data from the PSX controller.  Begin
        // by segragating the SPI message sequence.
        SPI_set_slave_addr( SPI_ADDR_NA );

        // Just collect the data and validate after.
        for( i = 0; i < MAX_PSXC_BYTES_TO_READ ; ++i )
        {

            psxc_response[ i ] = 
                            SPI_receive( SPI_ADDR_PSXC, psxc_poll_seq[ i ] );

            // Wait a bit before reading the next one.
            DELAY_us( 50 );

            if ( i >= 1 )
            {

                // If the data stream is digital-mode data, stop after the 
                // 5th byte.
                if ( ( i == 4 ) && ( psxc_response[ 1 ] == DIGITAL_MODE_BYTE ) )

                    break;

                // Otherwise, if data stream is analog-mode data, stop after
                // the 9th byte.
                else if ( ( i == 8 ) && 
                          ( psxc_response[ 1 ] == ANALOG_MODE_BYTE ) )

                    break;

            } // end if()

        } // end for()

        // Mark end of transaction with PSXC.
        SPI_set_slave_addr( SPI_ADDR_NA );

        // Validate and determine the data type.
        if ( psxc_response[ 2 ] == REPLY_BYTE )
        {

            // Set the appropriate data mode.
            if ( psxc_response[ 1 ] == DIGITAL_MODE_BYTE )

                pStatus_data->data_type = PSXC_DIGITAL;

            else if ( psxc_response[ 1 ] == ANALOG_MODE_BYTE )
            {

                pStatus_data->data_type = PSXC_ANALOG;

                // Adjust the analog parameters to map all values from
                // the native 0-255 range to -128 to 127 range with zero
                // being the 'middle', to make the analog values more 
                // consistent and logically meaningful (you'll have to do
                // this at some point anyway -- or the user will).
                //
                // left-right:
                psxc_response[ 5 ] -= 128;
                psxc_response[ 7 ] -= 128;

                // up-down:
                psxc_response[ 6 ] = ( ~psxc_response[ 6 ] ) - 128;
                psxc_response[ 8 ] = ( ~psxc_response[ 8 ] ) - 128;


                // Also provide the 'analog' parameters.
                ( pStatus_data->left_joy ).up_down      = psxc_response[ 8 ];
                ( pStatus_data->left_joy ).left_right   = psxc_response[ 7 ];

                ( pStatus_data->right_joy ).up_down     = psxc_response[ 6 ];
                ( pStatus_data->right_joy ).left_right  = psxc_response[ 5 ];

                // If this is the first analog read and the current 
                // center values are invalidated, then we need to save these.
                if ( PSXC_params.center.valid == FALSE )
                {

                    PSXC_params.center.left_joy.up_down = psxc_response[ 8 ];
                    PSXC_params.center.left_joy.left_right = 
                                                          psxc_response[ 7 ];

                    PSXC_params.center.right_joy.up_down = psxc_response[ 6 ];
                    PSXC_params.center.right_joy.left_right = 
                                                           psxc_response[ 5 ];

                    PSXC_params.center.valid = TRUE;

                } // end if()

            } // end else-if().

            // Otherwise we still have invalid data, even though the first
            // byte was considered good.
            else
            {

                pStatus_data->data_type = PSXC_INVALID;

                PSXC_params.center.valid = FALSE;

                rval = FALSE;

            } // end else.

            // Fill the structure with pertinent button/dpad/shoulder data.
            pStatus_data->buttons0 = psxc_response[ 3 ];
            pStatus_data->buttons1 = psxc_response[ 4 ];

        } // end if()

        // Mark that the current data structure as invalid and 
        // also invalidate the internal 'center' values for analog parameters.
        else
        {

            pStatus_data->data_type = PSXC_INVALID;

            PSXC_params.center.valid = FALSE;

            // Return 'no-good' value.
            rval = FALSE;

        } // end else.


    } // end if()
   
    // That's it.
    return rval;
    
} // end PSXC_read()
