/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
// Auth: Jose Santos
// Desc: Implementation file for 'STEPPER_step()' function.

#include "step324v221.h"
#include "__step324v221.h"

void STEPPER_step( STEPPER_ID which, STEPPER_DIR dir,
                   unsigned short int nSteps, unsigned short int nStepsPerSec,
                   STEPPER_BRKMODE onStopDoWhat, STEPPER_WAITMODE wait_mode,
                   STEPPER_NOTIFY *pNotifyFlag )
{

    static STEPPER_NOTIFY notify;   // Used when 'wait_mode = STEPPER_WAIT'.

    // Set the operating run-mode to step mode.
    STEPPER_set_mode( which, STEPPER_STEP_MODE );

    // If acceleration is enabled (ie., nonzero), then we need to compute
    // the deceleration step value (e.g., the step value where deceleration
    // should begin ).  We do so separately for each stepper.
    STEPPER_setup_decels( which, nSteps, nStepsPerSec );

    // Set the direction.
    STEPPER_set_dir( which, dir );

    // Set the stop mode.
    STEPPER_set_stop_mode( which, onStopDoWhat );
    
    // Set the number of steps.
    STEPPER_set_steps( which, nSteps );

    // Reset the notify values.
    if ( pNotifyFlag != NULL )
    {

        switch( which )
        {

            case LEFT_STEPPER:

                pNotifyFlag->left = 0;
                STEPPER_params.pending.left = 0;

                break;

            case RIGHT_STEPPER:

                pNotifyFlag->right = 0;
                STEPPER_params.pending.right = 0;
                
                break;

            case BOTH_STEPPERS:

                pNotifyFlag->left  = 0;
                pNotifyFlag->right = 0;

                STEPPER_params.pending.left  = 0;
                STEPPER_params.pending.right = 0;

                break;

        } // end switch()

    } // end if()

    // Register the user-supplied notification flag if any (whether 'NULL'
    // or not ).
    STEPPER_params.pNotify = pNotifyFlag;

    // That's it.

    // Now determine if we need to 'wait' based on 'wait_mode'.  If so...
    if ( wait_mode == STEPPER_WAIT )
    {

        // We'll assign our own [local] notify flag.
        STEPPER_params.pNotify = &notify;

        // Set the step speed and GO!
        STEPPER_set_speed( which, nStepsPerSec );

        // Change the 'brake' status so that we can process the next 
        // step request.
        STEPPER_go( which );

        // We'll need to busy wait depending on which stepper is active.
        switch( which )
        {

            case LEFT_STEPPER:

                // Ensure the flag is cleared.
                notify.left = 0;
                STEPPER_params.pending.left = 0;

                // Wait for step completion for left motor.
                while( ! ( notify.left ) ); 

                break;

            case RIGHT_STEPPER:

                // Ensure the flag is cleared.
                notify.right = 0;
                STEPPER_params.pending.right = 0;

                // Wait for step completion for right motor.
                while( ! ( notify.right ) );

                break;

            case BOTH_STEPPERS:

                // Ensure flags are both cleared.
                notify.left  = 0;
                notify.right = 0;

                STEPPER_params.pending.left  = 0;
                STEPPER_params.pending.right = 0;

                // Wait for step completion for BOTH motors.
                while( ( ! ( notify.left ) ) || ( ! ( notify.right ) ) );

                break;

        } // end switch()

    } // end if()

    // Otherwise, just set the speed and go.
    else
    {

        // Set the step speed and GO!
        STEPPER_set_speed( which, nStepsPerSec );

        // Change the 'brake' status so that we can process the next 
        // step request.
        STEPPER_go( which );

    } // end else.

} // end STEPPER_step()
